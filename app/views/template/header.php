<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title><?= $data['judul'] ?></title>
		<link rel="stylesheet" href="<?= BASEURL ?>/css/bootstrap.css">
		<link rel="stylesheet" href="<?= BASEURL ?>/jquery/jquery-3.3.1.min.js">
	</head>
	<body>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item active">
						<a class="nav-link" href="<?= BASEURL ?>">Home <span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?= BASEURL ?>/obat">obat</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?= BASEURL ?>/jenis">Jenis Obat</a>
					</li>
				</ul>
			</div>
		</nav>
		</div>