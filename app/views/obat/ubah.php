<!DOCTYPE HTML>
<html>
<head>
	<title>
		Obat
	</title>
</head>
<body>
	<div class="container">
				<form action="<?= BASEURL; ?>/Obat/edit" method="post">
				<input type="hidden" name="id" value="<?= $data['obat']['id'] ?>">
				  <div class="form-group">
					<label for="nama"  class="col-form-label">Nama Obat</label>
					<input type="text" name="nama" id="nama" class="form-control" value="<?= $data['obat']['nama_obat'] ?>">
				  </div>
				  <div class="form-group">
					<label for="jenis" class="col-form-label">Jenis Obat</label>
					<select name="jenis" class="form-control">
					<?php foreach ($data['jenis'] as $key) : ?>
						<option value="<?= $key['id_jenis'] ?>">
							<?= $key['nama_jenis'] ?>
						</option>
					<?php endforeach; ?>
					</select>
				  </div>
				  <div class="form-group">
					<label for="stok" class="col-form-label">Stok Obat</label>
					<input type="number" name="stok" id="stok"  class="form-control" value="<?= $data['obat']['stok_obat'] ?>">
				  </div>
				  <div class="form-group">
					<label for="tgl" class="col-form-label">Tanggal Kadaluarsa</label>
					<input type="date" name="tgl" id="tgl"  class="form-control" value="<?= $data['obat']['tgl_exp'] ?>">
				  </div>
			  <div class="form-group">
				<button type="submit" name="ubah" class="btn btn-primary">Ubah</button>
			  </div>
			 </form>
		</div>
	</div>
</body>
</html>