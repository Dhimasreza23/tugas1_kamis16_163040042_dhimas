<!DOCTYPE HTML>
<html>

<head>
	<title>
		Obat
	</title>
</head>
<body>
	<div class="container">
		<br>
		<h2 class="text-center">Daftar Obat</h2>
		<div class="col-5">

			<form action="<?= BASEURL; ?>/Obat/cari" method="post">
				<div class="form-group">
					<th>
						
					<table>
						<tr>
							<th><input type="text" name="nama" id="nama" class="form-control"></th>
							<th><button type="submit" name="cari" class="btn btn-primary">Cari</button></th>
							<th>
								<button type="button" class="btn btn-primary " data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Tambah</button>
							</th>
							
						</tr>
					</table>
				</div>
			</form>
		</div>
		<table class="table">
		  <thead>
			<tr>
			  <th scope="col">Id</th>
			  <th scope="col">Nama Obat</th>
			  <th scope="col">Jenis Obat</th>
			  <th scope="col">Stok Obat</th>
			  <th scope="col">Tanggal Expired</th>
			  <th scope="col">Aksi</th>
			</tr>
		  </thead>
		  <tbody>
		  	
		  <?php foreach ($data['obat'] as $key1) : ?>
			<tr>
			  <th scope="row"><?= $key1["id"]; ?></th>
			  <td><?= $key1["nama_obat"]; ?></td>
			  <td><?= $key1["nama_jenis"]; ?></td>
			  <td><?= $key1["stok_obat"]; ?></td>
			  <td><?= $key1["tgl_exp"]; ?></td>
			  <td>
			  		<a href="<?= BASEURL; ?>/Obat/ubah/<?= $key1['id']; ?>" class="btn btn-primary btn-sm" >Ubah</a>
			  		<a href="<?= BASEURL; ?>/Obat/hapus/<?= $key1['id']; ?>" class="btn btn-danger btn-sm">hapus</a>

			  </td>
			</tr>
			<tr>
			
			<!--
					<form action="<?= BASEURL; ?>/Obat/edit" method="post">
					<input type="hidden" class="form-control" name="id" value="<?= $key1['id'] ?>">
						<td></td>
						<td> <div class="form-group">
							<label for="nama"  class="col-form-label">Nama Obat</label>
							<input type="text" name="nama" id="nama" class="form-control" value="<?= $key1['nama_obat'] ?>">
						  </div></td>
						<td> <div class="form-group">
							<label for="jenis" class="col-form-label">Jenis Obat</label>
							<select name="jenis" class="form-control">
							<?php foreach ($data['jenis'] as $key) : ?>
								<option value="<?= $key['id_jenis'] ?>">
									<?= $key['nama_jenis'] ?>
								</option>
							<?php endforeach; ?>

							</select>
						  </div></td>
						<td> <div class="form-group">
							<label for="stok" class="col-form-label">Stok Obat</label>
							<input type="number" name="stok" id="stok"  class="form-control" value="<?= $key1['stok_obat'] ?>">
						  </div></td>
						<td> 
						  <div class="form-group">
							<label for="tgl" class="col-form-label">Tanggal Kadaluarsa</label>
							<input type="date" name="tgl" id="tgl"  class="form-control" value="<?= $key1['tgl_exp'] ?>">
						  </div></td>
						<td>	
					  <div class="form-group">
						<button type="submit" name="ubah" class="btn btn-primary">Ubah</button>
					  </div></td>
				   </form>
		-->
			
			</tr>
		  <?php endforeach; ?>
		  </tbody>
		</table>


		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tambah Obat</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>
			  <div class="modal-body">

				<form action="<?= BASEURL; ?>/Obat/insert" method="post">
				  <div class="form-group">
					<label for="nama"  class="col-form-label">Nama Obat</label>
					<input type="text" name="nama" id="nama" class="form-control">
				  </div>

				  <div class="form-group">
					<label for="jenis" class="col-form-label">Jenis Obat</label>
					<select name="jenis" class="form-control">
					<?php foreach ($data['jenis'] as $key1) : ?>
						<option value="<?= $key['id_jenis'] ?>">
							<?= $key1['nama_jenis'] ?>
						</option>
					<?php endforeach; ?>
					</select>
				  </div>

				  <div class="form-group">
					<label for="stok" class="col-form-label">Stok Obat</label>
					<input type="number" name="stok" id="stok"  class="form-control">
				  </div>

				  <div class="form-group">
					<label for="tgl" class="col-form-label">Tanggal Kadaluarsa</label>
					<input type="date" name="tgl" id="tgl"  class="form-control">
				  </div>

			  </div>
			  <div class="modal-footer">
				<button type="submit" name="submit" class="btn btn-primary">Simpan</button>
			  </div>
			 </form>

			</div>
		  </div>
		</div>
	</div>


  


</body>
</html>

