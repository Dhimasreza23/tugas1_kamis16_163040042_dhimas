<!DOCTYPE HTML>
<html>
<head>
	<title>
		Jenis
	</title>
</head>
<body>
	<div class="container">
		<br>
		<h2 class="text-center">Jenis Obat</h2>
		
		<div class="col-5">
			<form action="<?= BASEURL; ?>/Jenis/cari" method="post">
				<div class="form-group">

					<table>
						<tr>
							<th><input type="text" name="nama" id="nama" class="form-control"></th>
							<th><button type="submit" name="cari" class="btn btn-primary">Cari</button></th>
							<th><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Tambah</button></th>
						</tr>
					</table>
				</div>
			</form>
		</div>
		<table class="table">
		  <thead>
			<tr>
			  <th scope="col">Id</th>
			  <th scope="col">Nama Jenis</th>
			  <th scope="col">Aksi</th>
			</tr>
		  </thead>
		  <tbody>
		  <?php foreach ($data['jenis'] as $key) : ?>
			<tr>
			  <th scope="row"><?= $key["id_jenis"]; ?></th>
			  <td><?= $key["nama_jenis"]; ?></td>
			  <td><a href="<?= BASEURL; ?>/Jenis/ubah/<?= $key['id_jenis']; ?>" class="btn btn-primary btn-sm">Ubah</a>
			  <a href="<?= BASEURL; ?>/Jenis/hapus/<?= $key['id_jenis']; ?>" class="btn btn-danger btn-sm">hapus</a></td>
			</tr>
		  <?php endforeach; ?>
		  </tbody>
		</table>
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tambah Jenis</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>
			  <div class="modal-body">
				<form action="<?= BASEURL; ?>/Jenis/insert" method="post">
				  <div class="form-group">
					<label for="nama"  class="col-form-label">Nama Jenis</label>
					<input type="text" name="nama" id="nama" class="form-control">
				  </div>
			  <div class="modal-footer">
				<button type="submit" name="submit" class="btn btn-primary">Simpan</button>
			  </div>
			 </form>
			</div>
		  </div>
		</div>
	</div>
</body>
</html>