<div class="container">
	<div class="jumbotron mt-4">
		<h1 class="display-4">Selamat Datang</h1>
		<hr class="my-4">
		<p>Klik tombol dibawah untuk melihat detail kelompok</p>
		<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Pelajari Lebih Lanjut</button>

	</div>
</div>

<div class="container">
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
        <h4 class="modal-title">Detail Kelompok</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body text-center">
        
          <h5>Ketua Kelompok</h5>
          <h6>Dhimas Reza F.</h6>
          <hr>
          <h5>Anggota Kelompok</h5>
          <h6>Azis Rianto</h6>
          <h6>Fajar Januar Wicaksi</h6>
          <h6>Farrel Devara</h6>
          <h6>Fadila Alya</h6>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
