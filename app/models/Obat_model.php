<?php
	class Obat_model{
		private $table = 'obat';
		private $db;
		
		public function __construct(){
			$this->db = new Database;
		}
							
		public function getAllObat($tabel){
			$this->table = $tabel;
			$this->db->query('SELECT * FROM '. $this->table);
			return $this->db->resultSet();
		}
		
		public function getElementById($table,$id){
			$this->table = $table;
			$this->db->query("SELECT * FROM ". $this->table ." WHERE id=:id");
			$this->db->bind("id",$id);
			return $this->db->single();
		}
		
		public function insert($tabel, $data = []){
			$this->table = $tabel;
			$this->db->query("insert into ". $this->table ." (nama_obat,id_jenis,stok_obat,tgl_exp)
			values(:nama,:jenis,:stok,:tgl)");
			$this->db->bind("nama", $data["nama"]);
			$this->db->bind("jenis", $data["jenis"]);
			$this->db->bind("stok", $data["stok"]);
			$this->db->bind("tgl", $data["tgl"]);
			$this->db->execute();
		}
		
		public function ubah($tabel, $data = []){
			$this->table = $tabel;
			$this->db->query("update ". $this->table ." set nama_obat=:nama, id_jenis=:jenis, stok_obat=:stok, tgl_exp=:tgl where id=:id");
			$this->db->bind("nama", $data["nama"]);
			$this->db->bind("jenis", $data["jenis"]);
			$this->db->bind("stok", $data["stok"]);
			$this->db->bind("tgl", $data["tgl"]);
			$this->db->bind("id", $data["id"]);
			$this->db->execute();
		}
		
		public function hapus($tabel, $id){
			$this->table = $tabel;
			$this->db->query("delete from ". $this->table .' where id=:id');
			$this->db->bind("id",$id);
			$this->db->execute();
		}
		
		public function cari($tabel, $data){
			$this->table = $tabel;
			$this->db->query("SELECT * FROM ". $this->table ." WHERE id=:cari OR nama_obat=:cari OR nama_jenis=:cari OR stok_obat=:cari OR tgl_exp=:cari");
			$this->db->bind("cari",$data);
			return $this->db->resultSet();
		}
	}