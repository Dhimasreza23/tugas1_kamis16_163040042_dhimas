<?php
	class Obat extends Controller{
		public function index(){
			$data = array(
					'judul' => 'Daftar Obat',
					'obat' => $this->model('Obat_model')->getAllObat("obat_view"),
					'jenis'=> $this->model('Obat_model')->getAllObat("jenis")
					);
			$this->view('template/header', $data);
			$this->view('obat/index',$data);
			$this->view('template/footer');
		}
		public function detail($id){
			$data = array(
					'judul' => 'Detail Obat',
					'obat' => $this->model('Obat_model')->getElementById("obat_view", $id)
					);
			$this->view('template/header', $data);
			$this->view('obat/detail',$data);
			$this->view('template/footer');
		}
		
		public function insert(){
			if(isset($_POST['submit'])){
				$data['nama'] = $_POST["nama"];
				$data['jenis'] = $_POST['jenis'];
				$data['stok'] = $_POST['stok'];
				$data['tgl'] = $_POST['tgl'];
				$this->model('Obat_model')->insert("obat", $data);
				$this->index();
			}
			
		}
		
		public function edit(){
			if(isset($_POST['ubah'])){
				$data['id'] = $_POST["id"];
				$data['nama'] = $_POST["nama"];
				$data['jenis'] = $_POST['jenis'];
				$data['stok'] = $_POST['stok'];
				$data['tgl'] = $_POST['tgl'];
				$this->model('Obat_model')->ubah("obat", $data);
				$this->index();
			}
			
		}
		
		public function ubah($id){
			$data = array(
					'judul' => 'Detail Obat',
					'obat' => $this->model('Obat_model')->getElementById('obat', $id),
					'jenis'=> $this->model('Obat_model')->getAllObat("jenis")
					);
			$this->view('template/header', $data);
			$this->view('obat/ubah',$data);
			$this->view('template/footer');
		}
		
		public function hapus($id){
			$this->model('Obat_model')->hapus("obat", $id);
			$this->index();
		}
		
		public function cari(){
			$nama = $_POST["nama"];
			if(isset($_POST["cari"]))
			$data = array(
					'judul' => 'Daftar Obat',
					'obat'=>$this->model('Obat_model')->cari("obat_view", $nama)
					);
			$this->view('template/header', $data);
			$this->view('obat/index',$data);
			$this->view('template/footer');
		}
	}