<?php
	class Jenis extends Controller{
		public function index(){
			$data = array(
					'judul' => 'Daftar Jenis',
					'obat' => $this->model('Obat_model')->getAllObat("obat_view"),
					'jenis'=> $this->model('Obat_model')->getAllObat("jenis")
					
					);
			$this->view('template/header', $data);
			$this->view('jenis/index',$data);
			$this->view('template/footer');
		}
		public function detail($id){
			$data = array(
					'judul' => 'Detail Jenis',
					'obat' => $this->model('Obat_model')->getElementById("jenis", $id)
					);
			$this->view('template/header', $data);
			$this->view('jenis/detail',$data);
			$this->view('template/footer');
		}
		
		public function insert(){
			if(isset($_POST['submit'])){
				$data['nama'] = $_POST["nama"];
				$this->model('Jenis_model')->insert("jenis", $data);
				$this->index();
			}
			
		}
		
		public function edit(){
			if(isset($_POST['ubah'])){
				$data['id'] = $_POST["id"];
				$data['nama'] = $_POST["nama"];
				$this->model('Jenis_model')->ubah("jenis", $data);
				$this->index();
			}
			
		}
		
		public function ubah($id){
			$data = array(
					'judul' => 'Detail Jenis',
					'jenis' => $this->model('Jenis_model')->getElementById('jenis', $id),
					);
			$this->view('template/header', $data);
			$this->view('jenis/ubah',$data);
			$this->view('template/footer');
		}
		
		public function cari(){
			$nama = $_POST["nama"];
			if(isset($_POST["cari"]))
			$data = array(
					'judul' => 'Daftar Jenis',
					'jenis'=>$this->model('Jenis_model')->cari("jenis", $nama)
					);
			$this->view('template/header', $data);
			$this->view('jenis/index',$data);
			$this->view('template/footer');
		}
		
		public function hapus($id){
			$this->model('Jenis_model')->hapus("jenis", $id);
			$this->index();
		}
	}